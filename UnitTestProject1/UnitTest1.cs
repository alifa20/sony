﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MvcApplication1.Models.DocumentTypes;
using Vega.USiteBuilder;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var parent = ContentHelper.GetByNodeId(1090);
            Post p = new Post();
            p.Name = "Added post1";
            p.Title = "Added post1";
            p.Description = "Description for the added post1";
            p.ParentId = parent.Id;
            p.umbracoUrlName = "/posts/all-posts/added-post1/";
            //p.Template = parent.GetChildren()[0].Template;
            ContentHelper.Save(p);
            Assert.AreSame(1, 1);
        }
    }
}
