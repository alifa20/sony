using System;
using Vega.USiteBuilder;

namespace MvcApplication1.Models.DocumentTypes
{
    [DocumentType(Name = "MasterTest", IconUrl = "folder.gif",
        AllowedTemplates = new string[] { },
        AllowedChildNodeTypes = new Type[] { })]
    public partial class MasterTest : DocumentTypeBase
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Umbraco URL Name", Description = "This property allows you to change the URL of the node without changing the name of the node/page you have created.", Tab = "Generic Properties", Mandatory = false)]
        public string umbracoUrlName { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Umbraco URL Alias", Description = "This property allows you to give the node multiple URLs using a textstring property. For example if we were to enter leemessenger,test/this-is-a-test this would resolve the following urls to the same page. /leemessenger.aspx /test/this-is-a-test.aspx", Tab = "Generic Properties", Mandatory = false)]
        public string umbracoUrlAlias { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.ContentPicker, Name = "Umbraco Redirect", Description = "Add the umbracoRedirect property alias to your document type with a content picker and you can then allow choose a node ID that you want the page to redirect to.", Tab = "Generic Properties", Mandatory = false)]
        public string umbracoRedirect { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Hide in Navi", Description = "This 'True/False' property will hide pages from the navigation when they are set to true.", Tab = "Generic Properties", Mandatory = false)]
        public string umbracoNaviHide { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.ContentPicker, Name = "Umbraco Internal Redirect ID", Description = "Add the umbracoInternalRedirectId property alias to your document type with a content picker and Umbraco will load the selected page�s content transparently; no url redirection", Tab = "Generic Properties", Mandatory = false)]
        public string umbracoInternalRedirectId { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.TrueFalse, Name = "Show In Footer", Description = "This options tells us whether to show the current page in the footer navigation.", Tab = "Generic Properties", Mandatory = false)]
        public string ShowInFooter { get; set; }


    }
}