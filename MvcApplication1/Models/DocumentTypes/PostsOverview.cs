﻿using System;
using System.Drawing;
using Vega.USiteBuilder;
using umbraco.cms.businesslogic.datatype;

namespace MvcApplication1.Models.DocumentTypes
{
    [DocumentType(Name = "Posts Overview", IconUrl = "settingTemplate.gif",
        AllowedTemplates = new string[] { "umbPostsOverview" },
        AllowedChildNodeTypes = new Type[] { })]
    public partial class PostsOverview : MasterTest
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Title", Description = "", Tab = "Content", Mandatory = true)]
        public string Title { get; set; }
    }
}