﻿using System;
using System.Drawing;
using Vega.USiteBuilder;
using umbraco.cms.businesslogic.datatype;

namespace MvcApplication1.Models.DocumentTypes
{
    [DocumentType(Name = "PostCat", IconUrl = "settingTemplate.gif",
        AllowedTemplates = new string[] { "umbPosts" },
        AllowedChildNodeTypes = new Type[] { })]
    public partial class PostCat : MasterTest
    {
        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Title", Description = "", Tab = "Content", Mandatory = true)]
        public string Title { get; set; }

        [DocumentTypeProperty(UmbracoPropertyType.Textstring, Name = "Description", Description = "", Tab = "Content", Mandatory = true)]
        public string Description { get; set; }
    }
}