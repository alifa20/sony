﻿using System;
using System.Drawing;
using Vega.USiteBuilder;
using umbraco.cms.businesslogic.datatype;

namespace MvcApplication1.Models.DocumentTypes
{
    [DocumentType(Name = "AllPosts", IconUrl = "settingTemplate.gif",
        AllowedTemplates = new string[] { "AllPosts" },
        AllowedChildNodeTypes = new Type[] { })]
    public partial class AllPosts : MasterTest
    {
       
    }
}