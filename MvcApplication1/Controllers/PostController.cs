﻿using MvcApplication1.Models.DocumentTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using umbraco.cms.businesslogic.web;
using Umbraco.Web.Models;
using Vega.USiteBuilder;

namespace MvcApplication1.Controllers
{
    public class PostController : Umbraco.Web.Mvc.SurfaceController
    {
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var parent = ContentHelper.GetByNodeId(1090);

            //Document d = Document.MakeNew("New node name", DocumentType.GetByAlias("myDoctype"), User.GetCurrent(), parentNodeId);
            Post p = new Post();
            p.Name = "Added post1";
            p.Title = "Added post1";
            p.Description = "Description for the added post1";
            p.ParentId = parent.Id;
            p.umbracoUrlName = "/posts/all-posts/added-post1/";
            //p.Template = parent.GetChildren()[0].Template;
            ContentHelper.Save(p);
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

    }
}
