﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="kelvinDigital.USiteBuilderAdmin.usercontrols_NestedRepeater" Codebehind="NestedRepeater.ascx.cs" %>
<asp:Repeater runat="server" id="rptList" OnItemDataBound="rptList_DataBound" Visible="False">
        <HeaderTemplate>
		<ul>
	</HeaderTemplate>
    <ItemTemplate>
        <li data="addClass: '<%# Eval("DocumentTypeStatus").ToString().ToLower() %>', icon: 'settingDataType.gif'" class='expanded <%# Eval("DocumentTypeStatus").ToString().ToLower() %>'>
            <span><asp:Literal runat="server" id="litAlias"></asp:Literal><%# Eval("Alias")%></span>
        <asp:PlaceHolder runat="server" id="plcChildren"></asp:PlaceHolder>
        </li>
    </ItemTemplate>
    <FooterTemplate>
        </ul>
    <script type="text/javascript">
        jQuery(function () {
            jQuery(".tree").dynatree({
                onActivate: function (node) {
                    // A DynaTreeNode object is passed to the activation handler
                    // Note: we also get this event, if persistence is on, and the page is reloaded.
                    console.log(node.data);
                },
                imagePath: "/umbraco/plugins/uSiteBuilderAdmin/skin/",
                persist: false
            });
        });
    </script>
    </FooterTemplate>
</asp:Repeater>


