﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="kelvinDigital.USiteBuilderAdmin.umbraco_plugins_uSiteBuilderAdmin_CompareResults" Codebehind="CompareResults.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="NestedRepeater" Src="~/umbraco/plugins/uSiteBuilderAdmin/NestedRepeater.ascx" %>
<style>
    div.box.same {
        background-color: #468847;
    }
    div.box.new {
        background-color: #0074CC;
    }
    div.box.changed {
        background-color: #C09853;
    }
    
    div.box {
        width: 15px;
        height: 15px;
        vertical-align: middle;
        display: inline-block;
        border: 1px solid #CCC;
        margin-right: 5px;
    }

    div.box + span {
        margin-right: 10px;
        margin-top: 2px;
    }

</style>

<asp:Panel runat="server" CssClass="tree imported-doctypes" ID="plcImported">
    <div style="float: left; border-bottom: 1px solid #DDD;width: 100%;padding-bottom: 10px;margin-top: -5px;">
        <div class="box same"></div><span>Unchanged</span>
        <div class="box new"></div><span>New</span>
        <div class="box changed"></div><span>Modified</span>
    </div>
    <div style="clear: left;"></div>
    <uc1:NestedRepeater ID="rptList" runat="server" />
</asp:Panel>
<div class="deleted-doctypes">
    <asp:Repeater runat="server" ID="rptDeleted" OnItemDataBound="rptDeleted_ItemDataBound"
        OnItemCommand="rptDeleted_ItemCommand">
        <HeaderTemplate>
            <h3>
                Deleted<br /><asp:Button runat="server" ID="btnDeleteAll" CssClass="btn btn-danger sync-button"
                    OnClick="btnDeleteAll_Click" Text="Delete all" /></h3>
            <ul class="dynatree-container">
        </HeaderTemplate>
        <ItemTemplate>
            <li>
                <img src="plugins/uSiteBuilderAdmin/skin/settingDataType.gif" alt="datatype">
                <span class="deleted">
                    <%# Eval("Alias")%></span>
                <asp:Button runat="server" CommandArgument='<%# Eval("Alias") %>' Text="Delete" CssClass="btn btn-danger sync-button"
                    ID="btnDelete" CommandName="Delete" />
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
</div>