﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="kelvinDigital.USiteBuilderAdmin.uSiteBuilderAdmin" CodeBehind="uSiteBuilderAdmin.ascx.cs" %>
<%@ Register Src="NestedRepeater.ascx" TagName="NestedRepeater" TagPrefix="uc1" %>
<%@ Register Src="ExportCode.ascx" TagName="ExportCode" TagPrefix="uc2" %>
<%@ Register Src="CompareResults.ascx" TagName="CompareResults" TagPrefix="uc3" %>
<link rel='stylesheet' type='text/css' href='plugins/uSiteBuilderAdmin/skin/ui.dynatree.css'>

<script src='plugins/uSiteBuilderAdmin/js/jquery.dynatree.min.js' type="text/javascript"></script>

<script src='plugins/uSiteBuilderAdmin/js/bootstrapSwitch.js' type="text/javascript"></script>

<style>
    ul.dynatree-container {
        border: 0;
        margin: 10px 0;
        padding: 10px 0 0;
    }

    .tree li > span.same a {
        background-color: #FFF;
        color: #468847;
    }


    .tree li > span.new a {
        background-color: #FFF;
        color: #0074CC;
    }

    .tree li > span.changed a {
        background-color: #FFF;
        color: #C09853;
    }

    .deleted-doctypes, .imported-doctypes {
        margin-top: 20px;
    }

        .deleted-doctypes .dynatree-container > li > span.deleted {
            color: #DA4F49;
        }

    .sync-button {
        margin-left: 0px;
    }

    .import-doctypes {
        min-height: 400px;
    }

    h3 input.btn {
        margin-bottom: 5px;
    }

    .dashboardWrapper h3 {
        border-bottom: 1px solid #DDD;
    }

    .tabbable {
        margin-top: 20px;
    }

    label.checkbox {
        float: left;
        margin-right: 20px;
    }
</style>
<link rel='stylesheet' type='text/css' href='//netdna.bootstrapcdn.com/twitter-bootstrap/2.0.4/css/bootstrap-combined.min.css'>
<link rel='stylesheet' type='text/css' href='plugins/uSiteBuilderAdmin/css/bootstrapSwitch.css'>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>

<script type="text/javascript">
    // create separate object for 1.7.2 version to not conflict with old version - used in the custom bootstrap-tabs loaded below
    window.jQuery172 = jQuery.noConflict(true);



</script>

<script type="text/javascript" src="plugins/uSiteBuilderAdmin/js/bootstrap-tabs.js"></script>

<div class="propertypane">
    <div class="propertyItem">
        <div class="dashboardWrapper">
            <div class="pull-right" style="width: 180px;">
                <label class="pull-left">Auto-sync:</label>
                <div id="auto" class="switch pull-right" data-on="danger" data-off="primary">
                    <asp:CheckBox runat="server" ID="chkAuto" />
                </div>
            </div>
            <ul class="nav nav-pills">
                <li class="active">
                    <a href="#import" data-toggle="tab">Import</a>
                </li>
                <li>
                    <a href="#export" data-toggle="tab">Export</a>
                </li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="import">
                    <div class="import-doctypes">
                        <div class="well clearfix">
                            <label class="checkbox">
                                <asp:CheckBox runat="server" ID="cbxDataTypes" Text="Data Types" Checked="true" />
                            </label>
                            <label class="checkbox">
                                <asp:CheckBox runat="server" ID="cbxDocTypes" Text="Document Types" Checked="true" />
                            </label>
                            <label class="checkbox">
                                <asp:CheckBox runat="server" ID="cbxTemplates" Text="Templates" Checked="true" />
                            </label>
                            <asp:Button runat="server" ID="btnCheck" CssClass="btn btn-primary" OnClick="btnCheck_Click"
                                Text="Check for changes" />
                        </div>
                        <asp:Button runat="server" ID="btnSyncAll" Visible="false"
                            CssClass="btn btn-danger sync-button" OnClick="btnSync_Click" Text="Sync all changes" />
                        <asp:PlaceHolder runat="server" ID="results" Visible="False">
                            <div class="tabbable">
                                <ul class="nav nav-tabs" runat="server" id="tabs">
                                    <li runat="server" id="dataTypeTab">
                                        <a href="#dataTypes" data-toggle="tab">Data Types</a>
                                    </li>
                                    <li runat="server" id="docTypeTab">
                                        <a href="#doctypes" data-toggle="tab">Document Types</a>
                                    </li>
                                    <li runat="server" id="templateTab">
                                        <a href="#templates" data-toggle="tab">Templates</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane" id="dataTypes" runat="server" clientidmode="Static">
                                        <h3>Imported Data Types</h3>
                                        <uc3:CompareResults ID="rsltDataTypes" OnItemDeleted="RebindGrids" runat="server" />
                                    </div>
                                    <div class="tab-pane" id="doctypes" runat="server" clientidmode="Static">
                                        <h3>Imported Document Types</h3>
                                        <uc3:CompareResults ID="rsltDocType" OnItemDeleted="RebindGrids" runat="server" />
                                    </div>
                                    <div class="tab-pane" id="templates" runat="server" clientidmode="Static">
                                        <h3>Imported Templates</h3>
                                        <uc3:CompareResults ID="rsltTemplates" OnItemDeleted="RebindGrids" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </asp:PlaceHolder>
                    </div>
                </div>
                <div class="tab-pane" id="export">
                    <uc2:ExportCode ID="ExportCode" runat="server" />
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    $('#auto').on('switch-change', function (e, data) {

        var on = data.value;

        var post = {
            type: "POST",
            url: "/umbraco/plugins/uSiteBuilderAdmin/uSiteBuilderChangeAutoRefresh.aspx?on=" + on
        };

        $.ajax(post);

    });
</script>

