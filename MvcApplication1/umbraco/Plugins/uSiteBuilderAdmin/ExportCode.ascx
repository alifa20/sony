﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="kelvinDigital.USiteBuilderAdmin.usercontrols_ExportCode" Codebehind="ExportCode.ascx.cs" %>
<%@ Register TagPrefix="cc1" Namespace="umbraco.uicontrols" Assembly="controls" %>
<link href="plugins/uSiteBuilderAdmin/css/prettify.css" type="text/css" rel="stylesheet" />
<style>
    code {
        background-color: #F2F2F7;
        font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,serif;
    }
    pre.prettyprint  {
        background-color: #F2F2F7;
        font-family: Consolas,Menlo,Monaco,Lucida Console,Liberation Mono,DejaVu Sans Mono,Bitstream Vera Sans Mono,Courier New,monospace,serif;
        margin-bottom: 10px;
        max-height: 600px;
        overflow: auto;
        padding: 5px;
        width: auto;
        height: 100%;
        border: 1px solid rgba(0, 0, 0, 0.15);
    }
    .prettyprint 
    {
        min-height: 400px; 
        width: 98%; 
    }
    
    div.export-namespace > div {
        margin-bottom: 10px;
        
    }
     
    div.export-namespace span {
        vertical-align: middle;
        margin-left: 20px;
    }

    div.export-namespace input[type=checkbox] + span {
        margin-left: 5px;
        display: inline-block;
        width: 100px;
    }

    div.export-namespace input[type=checkbox]{
        vertical-align: middle;
        margin: 0;
    }
    
</style>

<script type="text/javascript" src="plugins/uSiteBuilderAdmin/js/prettify.js"></script>

<script type="text/javascript">

    jQuery(function () {
        jQuery("div.export-namespace input[type=checkbox]").click(function () {
            if ($(this).is(':checked')) {
                $(this).siblings('input[type=text]').removeAttr('disabled');
            } else {
                $(this).siblings('input[type=text]').attr('disabled', 'disabled');
            }
        });
    });

</script>

<div>
    <div class="export-namespace input-append well">
        <div>
            <asp:CheckBox Checked="true" runat="server" id="cbxExportDataTypes" /><asp:Label ID="Label1" runat="server" Text="Data Types"></asp:Label>
            <asp:Label ID="lbl" runat="server" Text="Namespace:"></asp:Label>
            <asp:TextBox ID="txtDataTypesNamespace" Width="300px" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:CheckBox Checked="true" runat="server" id="cbxExportDocTypes" /><asp:Label ID="Label2" runat="server" Text="Document Types"></asp:Label>
            <asp:Label ID="lblDoctypesNamespace" runat="server" Text="Namespace:"></asp:Label>
            <asp:TextBox ID="txtDocTypesNamespace" Width="300px" runat="server"></asp:TextBox>
        </div>
        <div>
            <asp:CheckBox Checked="true" runat="server" id="cbxExportTemplates" /><asp:Label ID="Label3" runat="server" Text="Templates"></asp:Label>
            <asp:Label ID="lblTemplatesNamespace" runat="server" Text="Namespace:"></asp:Label>
            <asp:TextBox ID="txtTemplatesNamespace" Width="300px" runat="server"></asp:TextBox>
        </div>

        <asp:Button runat="server" id="btnPreview" OnClick="preview_Click" Text="Preview" CssClass="btn btn-info" />
        <asp:Button runat="server" id="btnSave" OnClick="save_Click" Text="Download" CssClass="btn btn-primary" />
    </div>
    <div class="export-buttons">
        <asp:PlaceHolder ID="plcButtons" runat="server"></asp:PlaceHolder>
    </div>
    <div class="export-code">
        <pre class="prettyprint">
             <code>
                <asp:Literal runat="server" ID="litCode"></asp:Literal>
            </code>
        </pre>
    </div>
</div>

<asp:PlaceHolder runat="server" id="activateTab" Visible="False" EnableViewState="false">
<script type="text/javascript">
    jQuery172('a[href=#export]').tab('show');
</script>
</asp:PlaceHolder>

<script type="text/javascript">

    jQuery(function () {
        prettyPrint();
    });

</script>

